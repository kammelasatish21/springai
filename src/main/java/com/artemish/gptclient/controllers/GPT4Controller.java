package com.artemish.gptclient.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class GPT4Controller {

	private String apiKey = "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";

	@Autowired
	private WebClient.Builder webClientBuilder;

	@PostMapping("/ask-gpt4")
	public Mono<String> askGPT4(@RequestParam String question,@RequestParam(required = false) List<MultipartFile> imageFiles) {
		WebClient webClient = webClientBuilder.baseUrl("https://api.openai.com/v1/chat/completions")
				.defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + apiKey)
				.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
				.defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE).build();

		// Creating the request payload
		Map<String, Object> textContent = new HashMap<>();
		textContent.put("type", "text");
		textContent.put("text", question);

		List<Map<String, Object>> contentList = new ArrayList<>();
		contentList.add(textContent);

		// Adding image content for each file
		if (imageFiles != null && !imageFiles.isEmpty()) {
			for (MultipartFile imageFile : imageFiles) {
				try {
					byte[] imageBytes = imageFile.getBytes();
					String base64Image =  Base64.getEncoder().encodeToString(imageBytes);

					Map<String, Object> imageContent = new HashMap<>();
					imageContent.put("type", "image_url");
					Map<String, String> imageUrlMap = new HashMap<>();
					imageUrlMap.put("url", "data:image/png;base64," + base64Image);
					imageContent.put("image_url", imageUrlMap);
					contentList.add(imageContent);
				} catch (IOException e) {
					e.printStackTrace();
					return Mono.error(new RuntimeException("Error processing the image file", e));
				}
			}
		}

		Map<String, Object> userMessage = new HashMap<>();
		userMessage.put("role", "user");
		userMessage.put("content", contentList);

		Map<String, Object> requestBody = new HashMap<>();
		requestBody.put("model", "gpt-4-turbo");
		requestBody.put("messages", List.of(userMessage));

		return webClient.post().body(BodyInserters.fromValue(requestBody)).retrieve().bodyToMono(String.class);
	}

	@Configuration
	class GPT4Configuration {
		@Bean
		WebClient.Builder gpt4WebClientBuilder() {
			return WebClient.builder();
		}
	}

}

